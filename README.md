# HOW TO USE

1\. Abra a pasta `api` e execute os comandos:

```bash
php artisan migrate
php artisan passport:install
```

2\. Anote o `ID` e o `Secret` do "Password Grant Client", e copie-os para os campos respectivos em `app/api.json`. Aproveite para ver o IP local do seu computador para colocá-lo no atributo `"base_url"`.

3\. Configure seu `.env`, abra um terminal na pasta `api/` e execute o servidor do Laravel com o comando:

```bash
php artisan serve --host 0.0.0.0
```

4\. Entre no site [`http://localhost:8000/`](http://localhost:8000/), e registre-se.

5\. Conecte seu celular ao computador via cabo USB e ative o *USB Debugging*.

6\. Abra dois terminais na pasta `app/` e execute `npm start` em um dos terminais para iniciar o servidor da aplicação e `react-native run-android` no outro terminal para compilar sua aplicação no celular.

**NOTA:** Ao executar `react-native run-android` pela primeira vez, seu celular pode bloquear a conexão e perguntar se você confia no dispositivo conectado. Selecione a opção "sempre permitir" e autorize o acesso. Se você demorar demais, o build irá falhar e você terá que re-executar o comando.

