/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput, View, Button, Modal, KeyboardAvoidingView} from 'react-native';
import { client_id as clientId, client_secret as clientSecret, base_url as baseUrl } from './api.json';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.verifyEcompStatus = this.verifyEcompStatus.bind(this);
    this.onSuccessfulLogin = this.onSuccessfulLogin.bind(this);
    this.onFailedLogin = this.onFailedLogin.bind(this);
    this.state = {
      status: null,
      login: {
        error: null,
        loggedIn: false,
        accessToken: null,
        refreshToken: null,
        tokenType: null,
        tokenExpiry: null,
      },
      loginModal: {
        visible: false,
      },
    };
  }

  verifyEcompStatus() {
    setTimeout(() => {
      let ecompStatus;
      if (Math.floor(Math.random() * 10) % 2 === 0) {
        ecompStatus = "Aberta";
      } else {
        ecompStatus = "Fechada";
      }

      this.setState({
        ...this.state,
        status: ecompStatus,
      });
    }, 200);
  }

  onSuccessfulLogin(r) {
    console.log("Success!");
    console.log(r);

    if (r.access_token != null) {
      this.setState({
        ...this.state,
        login: {
          ...this.state.login,
          error: null,
          loggedIn: true,
          accessToken: r.access_token,
          refreshToken: r.refresh_token,
          tokenType: r.token_type,
          tokenExpiry: r.expires_in,
        }
      });
    }

    this.hideModalForLogin();
  }

  onFailedLogin(e) {
    console.log("Error!");
    console.log(e);

    this.setState({
      ...this.state,
      login: {
        ...this.state.login,
        error: e.message,
      },
    });

    this.hideModalForLogin();
  }

  showModalForLogin() {
    this.setState({
      ...this.state,
      loginModal: {
        ...this.state.loginModal,
        visible: true,
      },
    });
  }

  hideModalForLogin() {
    this.setState({
      ...this.state,
      loginModal: {
        ...this.state.loginModal,
        visible: false,
      },
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Ecomp Aberta?</Text>
        <LoginModal loggedIn={this.state.login.loggedIn} visible={this.state.loginModal.visible} onSuccess={(r) => this.onSuccessfulLogin(r)} onFailure={(e) => this.onFailedLogin(e)} onClickClose={() => this.hideModalForLogin()} />
        <LoginStatus loggedIn={this.state.login.loggedIn} error={this.state.login.error} buttonHandler={() => this.showModalForLogin()} />
        <EcompStatus loggedIn={this.state.login.loggedIn} status={this.state.status} statusHandler={() => this.verifyEcompStatus()} />
      </View>
    );
  }
}

class LoginModal extends Component<Props> {
  constructor(props) {
    super(props);
    this.onSuccess = this.props.onSuccess || function() {};
    this.onFailure = this.props.onFailure || function() {};
    this.onClickClose = this.props.onClickClose || function() {};
    this.updateUsername = this.updateUsername.bind(this);
    this.updatePassword = this.updatePassword.bind(this);
    this.attemptLogin = this.attemptLogin.bind(this);
    this.state = {
      username: "",
      password: "",
    };
  }

  attemptLogin() {
    var onSuccess = this.onSuccess;
    var onFailure = this.onFailure;

    console.log("Attempting login with: ", this.state);

    if (!this.props.loggedIn) {
      fetch(`${baseUrl}/oauth/token`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
          grant_type: "password",
          client_id: clientId,
          client_secret: clientSecret,
          scope: "",
        }),
      })
        .then((r) => r.json())
        .then((r) => {
          console.log(r);
          if (r.error != null) {
            console.log("Calling failure handler");
            onFailure(r);
          } else {
            console.log("Calling success handler");
            onSuccess(r);
          }
        })
        .catch((e) => onFailure(e));
    }
  }

  updateUsername(username) {
    this.setState({
      ...this.state,
      username,
    });
  }

  updatePassword(password) {
    this.setState({
      ...this.state,
      password,
    })
  }

  render() {
    return (
      <Modal animationType="slide" transparent={false} visible={this.props.visible} onRequestClose={() => this.onClickClose()}>
        <KeyboardAvoidingView style={styles.container}>
          <View style={styles.loginModalView}>
            <Text>Usuário:</Text>
            <TextInput
              style={styles.input}
              placeholder="Seu usuário"
              onChangeText={(text) => this.updateUsername(text)}
              value={this.state.username}
              onSubmitEditing={() => this.refs.passwordInput.focus()}
              blurOnSubmit={false}
              returnKeyType="next" />
            <Text>Senha:</Text>
            <TextInput
              style={styles.input}
              ref="passwordInput"
              secureTextEntry
              placeholder="Sua senha"
              onChangeText={(text) => this.updatePassword(text)}
              value={this.state.password}
              onSubmitEditing={() => this.attemptLogin()}
              returnKeyType="go" />

            <View style={styles.inlineButtons}>
              <Button title="Entrar" onPress={() => this.attemptLogin()} />
              <Button title="Fechar" color="#AA2222" onPress={() => this.onClickClose()} />
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

const LoginStatus = ({ error, loggedIn, buttonHandler, accessToken }) => {
  return (
    <View style={styles.statusView}>
      <ShowLoginStatusOrError loggedIn={loggedIn} error={error} />
      <View style={styles.button}>
        <Button title="Entrar" onPress={() => buttonHandler()} />
      </View>
    </View>
  );
};

const ShowLoginStatusOrError = ({ error, loggedIn }) => {
  if (error != null) {
    return <ErrorMessage error="Credenciais inválidas!" />;
  } else if (!loggedIn) {
    return <Text>Você não está logado :(</Text>;
  } else {
    return <Text>Você está logado :)</Text>
  }
}

class EcompStatus extends Component<Props> {
  constructor(props) {
    super(props);
    this.statusHandler = this.props.statusHandler || function() {};
    this.validateUserIsLoggedIn = this.validateUserIsLoggedIn.bind(this);
    this.state = {
      error: null
    };
  }

  validateUserIsLoggedIn() {
    if (!this.props.loggedIn) {
      this.setState({
        ...this.state,
        error: "Você deve estar logado para verificar o status!",
      });
    } else {
      if (this.state.error != null) {
        this.setState({
          ...this.state,
          error: null,
        });
      }
      this.statusHandler();
    }
  }

  render() {
    return (
      <View style={{marginTop: 10, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <ShowStatus status={this.props.status} error={this.state.error} />
        <View style={styles.button}>
          <Button title="Verificar" onPress={() => this.validateUserIsLoggedIn()}/>
        </View>
      </View>
    );
  }
}

const ShowStatus = ({ status, error }) => {
  if (error != null) {
    return <ErrorMessage error={error} />;
  } else if (status != null) {
    return <Text>Status: {status}</Text>;
  } else {
    return null;
  }
};

const ErrorMessage = ({ error }) => {
  if (error != null) {
    return  <Text style={{color: 'red'}}>{error}</Text>;
  } else {
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  statusView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  loginModalView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
  },
  input: {
    width: '100%',
    margin: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: '#555555',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 4,
  },
  welcome: {
    fontSize: 26,
    textAlign: 'center',
    margin: 10,
  },
  inlineButtons: {
    margin: 15,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '80%',
  },
  button: {
    justifyContent: 'center',
    margin: 15,
    width: '70%',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
